# auto-mirroring

Demo of a concept for automatic project mirroring

This project contains a pipeline that 
* Checks if a project with the same name already exists on secondary GitLab instance
* If it does not exist, an automatic pull mirror is created
* If it already exists, mirroring process is triggered via API
